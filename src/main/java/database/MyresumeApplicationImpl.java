package database;

import database.generated.GeneratedMyresumeApplicationImpl;

/**
 * The default {@link com.speedment.runtime.core.Speedment} implementation class
 * for the {@link com.speedment.runtime.config.Project} named myresume.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class MyresumeApplicationImpl 
extends GeneratedMyresumeApplicationImpl 
implements MyresumeApplication {}