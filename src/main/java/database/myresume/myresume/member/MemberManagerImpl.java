package database.myresume.myresume.member;

import database.myresume.myresume.member.generated.GeneratedMemberManagerImpl;

/**
 * The default implementation for the manager of every {@link
 * database.myresume.myresume.member.Member} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class MemberManagerImpl 
extends GeneratedMemberManagerImpl 
implements MemberManager {}