package database.myresume.myresume.member;

import database.myresume.myresume.member.generated.GeneratedMemberManager;

/**
 * The main interface for the manager of every {@link
 * database.myresume.myresume.member.Member} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public interface MemberManager extends GeneratedMemberManager {}