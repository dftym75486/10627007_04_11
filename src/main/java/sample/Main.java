package sample;

import com.speedment.runtime.core.ApplicationBuilder;
import database.MyresumeApplication;
import database.MyresumeApplicationBuilder;
import database.myresume.myresume.member.Member;
import database.myresume.myresume.member.MemberImpl;
import database.myresume.myresume.member.MemberManager;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.sql.Date;
import java.time.LocalDate;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/sample.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();


        MyresumeApplication myresumeApplication = new MyresumeApplicationBuilder()
                .withUsername("root").withPassword("abcd1234")
                .withLogging(ApplicationBuilder.LogType.CONNECTION)
                .withLogging(ApplicationBuilder.LogType.PERSIST)
                .withLogging(ApplicationBuilder.LogType.REMOVE)
                .build();

        MemberManager memberManager = myresumeApplication.getOrThrow(MemberManager.class);
        Member member = new MemberImpl().setName("中文").setMobile("0988878787").setBirthplace("Taiwan")
                .setBirthday(Date.valueOf("2000-01-01"));

        memberManager.persist(member);
    }


    public static void main(String[] args) {
        launch(args);
    }
}
