package sample;

import com.speedment.runtime.core.ApplicationBuilder;
import database.MyresumeApplication;
import database.MyresumeApplicationBuilder;
import database.myresume.myresume.member.Member;
import database.myresume.myresume.member.MemberImpl;
import database.myresume.myresume.member.MemberManager;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Controller {
    public Button btnAdd;
    public TextField txtMobile;
    public TextField txtDel;
    public Button btnDel;

    public void onAdd(ActionEvent actionEvent) {
        String mobile = txtMobile.getText();
        if (mobile.length() == 10) {

            MyresumeApplication myresumeApplication = new MyresumeApplicationBuilder()
                    .withUsername("root").withPassword("abcd1234")
                    .withLogging(ApplicationBuilder.LogType.CONNECTION)
                    .withLogging(ApplicationBuilder.LogType.PERSIST)
                    .withLogging(ApplicationBuilder.LogType.REMOVE)
                    .build();

            MemberManager memberManager = myresumeApplication.getOrThrow(MemberManager.class);

            Member member = new MemberImpl().setName("中文").setMobile(mobile).setBirthplace("Taiwan")
                    .setBirthday(Date.valueOf("2000-01-01"));

            memberManager.persist(member);
        }
        else
        {
         System.out.println("Exceed 10");
        }
    }

    public void onDel(ActionEvent actionEvent) {
        String mobile = txtMobile.getText();
            MyresumeApplication myresumeApplication = new MyresumeApplicationBuilder()
                    .withUsername("root").withPassword("abcd1234")
                    .withLogging(ApplicationBuilder.LogType.CONNECTION)
                    .withLogging(ApplicationBuilder.LogType.PERSIST)
                    .withLogging(ApplicationBuilder.LogType.REMOVE)
                    .build();

            MemberManager memberManager = myresumeApplication.getOrThrow(MemberManager.class);

            List<Member> memberList = memberManager.stream().filter(Member.MOBILE.equal(mobile)).collect(Collectors.toList());

            memberList.forEach(
                    memberManager::remove
            );
           }
    }
